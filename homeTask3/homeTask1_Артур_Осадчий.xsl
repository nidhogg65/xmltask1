<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    
    <xsl:template match="string">
        <Guests>
            <xsl:call-template name="split">
                <xsl:with-param name="string" select="."/>
            </xsl:call-template>
        </Guests>
    </xsl:template>
    
    <xsl:template name="split">
        <xsl:param name="string"/>
        
        <xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($string, ',')"/></xsl:variable>
        <xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
        <xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
        <xsl:variable name="attr_guest5"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
        <xsl:variable name="attr_guest6"><xsl:value-of select="substring-after($attr_guest5, ',')"/></xsl:variable>
        
        <Guest Age="{substring-before($string, ',')}"
               Nationalty="{substring-before($attr_guest2, ',')}"
               Gender="{substring-before($attr_guest3, ',')}"
               Name="{substring-before($attr_guest4, ',')}">
            <Type>
                <xsl:value-of select="substring-before($attr_guest5, ',')"/>
            </Type>
            <Profile>
                <Address>
                    <xsl:value-of select="substring-before($attr_guest6, '|')"/>
                </Address>
            </Profile>
        </Guest>
        <xsl:choose>
            <xsl:when test="boolean(substring-after($string, '|'))"> 
                <xsl:call-template name="split">
                    <xsl:with-param name="string" select="substring-after($string, '|')"/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>