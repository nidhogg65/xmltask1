<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    
    <xsl:key name="unique_group" match="*|@*" use="name(.)"/>
    
    <xsl:template match="*|@*">       
        <xsl:choose>
            <!--Если элемент/атрибут является последним в своей группе, то выводим количество этих элементов,
                в противном случае происходит проверка на то, является ли этот элементом последним в xml документе-->
            <xsl:when test="generate-id(.)=generate-id(key('unique_group', name(.))[last()])">
                <xsl:value-of select="concat('Node ', name(.), ' found ', count(key('unique_group', name(.))), ' times', '&#x0A;')"/>
                <xsl:if test="position() &lt; count(//*|//@*)">
                    <xsl:apply-templates select="*|@*"/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="*|@*"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template> 

</xsl:stylesheet>