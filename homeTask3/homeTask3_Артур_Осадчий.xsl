<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    
    <xsl:key name="ITEM_group" match="item" use="substring(@Name, 1, 1)"/>
    
    <xsl:template match="list">
        <list>
            <xsl:apply-templates select="item[generate-id(.)=generate-id(key('ITEM_group',substring(@Name, 1, 1)))]">
                <xsl:sort select="substring(@Name, 1, 1)"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>
    
    <xsl:template match="item">
        <capital value="{substring(@Name, 1, 1)}">
            <xsl:for-each select="key('ITEM_group', substring(@Name, 1, 1))">
                <xsl:sort select="@Name"/>
                <name>
                    <xsl:value-of select="@Name"/>
                </name>
            </xsl:for-each>
        </capital>                
    </xsl:template>       
</xsl:stylesheet>