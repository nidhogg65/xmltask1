<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://soapserver.com/entities"
                xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
                xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
                xmlns:tostring="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.NodeSetToStringUtil"
                exclude-result-prefixes="ent sutil queryutil tostring">

    <xsl:output method="xml"/>

    <xsl:template match="/">
        <xsl:variable name="ids" select="tostring:toString(Result/Entry/hotel_id)"/>
        <xsl:variable name="images" select="queryutil:subQuery(
                concat('select hi.hotel_id, hi.url, hit.type_name from gpt_hotel_images hi ',
                 'join gpt_hotel_image_type hit on hi.type_id=hit.id ',
                  'where hi.hotel_id in (', $ids, ')'))"/>

        <ent:HotelsResponse>
            <xsl:for-each select="Result/Entry">
                <xsl:variable name="country" select="country_name"/>
                <xsl:variable name="countryCode" select="country_code"/>
                <xsl:variable name="city" select="city_name"/>
                <xsl:variable name="cityCode" select="city_code"/>
                <xsl:variable name="hotelName" select="hotel_name"/>
                <xsl:variable name="hotelCode" select="hotel_code"/>
                <xsl:variable name="phone" select="phone"/>
                <xsl:variable name="fax" select="fax"/>
                <xsl:variable name="email" select="email"/>
                <xsl:variable name="url" select="url"/>
                <xsl:variable name="checkIn" select="check_in"/>
                <xsl:variable name="checkOut" select="check_out"/>
                <xsl:variable name="longitude" select="longitude"/>
                <xsl:variable name="latitude" select="latitude"/>

                <xsl:variable name="hotelId" select="hotel_id"/>


                <ent:Hotel>
                    <ent:Country>
                        <ent:Name><xsl:value-of select="$country"/></ent:Name>
                        <ent:Code><xsl:value-of select="$countryCode"/></ent:Code>
                    </ent:Country>
                    <ent:City>
                        <ent:Name><xsl:value-of select="$city"/></ent:Name>
                        <ent:Code><xsl:value-of select="$cityCode"/></ent:Code>
                    </ent:City>
                    <ent:HotelInfo>
                        <ent:Name><xsl:value-of select="$hotelName"/></ent:Name>
                        <ent:Code><xsl:value-of select="$hotelCode"/></ent:Code>
                    </ent:HotelInfo>
                    <ent:Phone>
                        <xsl:value-of select="$phone"/>
                    </ent:Phone>
                    <ent:Fax>
                        <xsl:value-of select="$fax"/>
                    </ent:Fax>
                    <ent:Email>
                        <xsl:value-of select="$email"/>
                    </ent:Email>
                    <ent:URL>
                        <xsl:value-of select="$url"/>
                    </ent:URL>
                    <ent:CheckIn>
                        <xsl:value-of select="$checkIn"/>
                    </ent:CheckIn>
                    <ent:CheckOut>
                        <xsl:value-of select="$checkOut"/>
                    </ent:CheckOut>
                    <ent:Coordinates>
                        <ent:Latitude><xsl:value-of select="$latitude"/></ent:Latitude>
                        <ent:Longitude><xsl:value-of select="$longitude"/></ent:Longitude>
                    </ent:Coordinates>
                    <ent:Images>
                        <xsl:call-template name="setImages">
                            <xsl:with-param name="images" select="$images"/>
                            <xsl:with-param name="id" select="$hotelId"/>
                        </xsl:call-template>
                    </ent:Images>
                </ent:Hotel>
            </xsl:for-each>
        </ent:HotelsResponse>
    </xsl:template>


    <xsl:template name="setImages">
        <xsl:param name="images"/>
        <xsl:param name="id"/>
        <xsl:for-each select="$images/Entry[hotel_id=$id]">
            <ent:Image type="{type_name}">
                <ent:URL><xsl:value-of select="url"/></ent:URL>
            </ent:Image>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>