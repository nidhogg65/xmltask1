<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://soapserver.com/entities"
                xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
                exclude-result-prefixes="ent requtil">

    <xsl:output method="text"/>

    <xsl:template match="/">
        <xsl:variable name="location" select="ent:HotelsRequest/ent:Location"/>
        <xsl:variable name="language" select="ent:HotelsRequest/ent:Language"/>
        <xsl:variable name="hotelName" select="ent:HotelsRequest/ent:PartOfHotelName"/>
        <xsl:variable name="category" select="ent:HotelsRequest/ent:HotelCategory"/>

        <xsl:text>
            select lnctr.name as country_name, ctr.code as country_code,
            lncty.name as city_name, cty.code as city_code, h.id as hotel_id,
            hn.name as hotel_name, h.code as hotel_code, h.phone, h.fax, h.email, h.url,
            h.check_in, h.check_out, h.longitude, h.latitude from gpt_hotel h

            join gpt_location_name lnctr on h.country_id = lnctr.location_id
            join gpt_location ctr on h.country_id = ctr.id

            join gpt_location_name lncty on h.city_id = lncty.location_id
            join gpt_location cty on h.city_id = cty.id

            join gpt_hotel_name hn on h.id = hn.hotel_id
        </xsl:text>

        <xsl:text>where ctr.code = '</xsl:text>
        <xsl:value-of select="$location/ent:CountryCode"/>
        <xsl:text>' </xsl:text>

        <xsl:if test="boolean($location/ent:City)">
            <xsl:text>and lncty.name = '</xsl:text>
            <xsl:value-of select="$location/ent:City"/>
            <xsl:text>' </xsl:text>
        </xsl:if>

        <xsl:text>and lnctr.lang_id = (select id from gpt_language where code = '</xsl:text>
        <xsl:value-of select="$language"/>
        <xsl:text>') </xsl:text>

        <xsl:text>and lncty.lang_id = (select id from gpt_language where code = '</xsl:text>
        <xsl:value-of select="$language"/>
        <xsl:text>') </xsl:text>

        <xsl:text>and hn.lang_id = (select id from gpt_language where code = '</xsl:text>
        <xsl:value-of select="$language"/>
        <xsl:text>') </xsl:text>

        <xsl:choose>
            <xsl:when test="boolean($hotelName) = false() and boolean($category) = false() ">
            <xsl:text>;</xsl:text>
            </xsl:when>

            <xsl:when test="boolean($hotelName) = true() and boolean($category) = false() ">
                <xsl:text>and hn.name like '%</xsl:text>
                <xsl:value-of select="$hotelName"/>
                <xsl:text>%';</xsl:text>
            </xsl:when>

            <xsl:when test="boolean($hotelName) = false() and boolean($category) = true() ">
                <xsl:text>and h.category_id = '</xsl:text>
                <xsl:value-of select="$category"/>
                <xsl:text>';</xsl:text>
            </xsl:when>

            <xsl:otherwise>
                <xsl:text>and hn.name like '%</xsl:text>
                <xsl:value-of select="$hotelName"/>
                <xsl:text>%' </xsl:text>

                <xsl:text>and h.category_id = '</xsl:text>
                <xsl:value-of select="$category"/>
                <xsl:text>';</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>