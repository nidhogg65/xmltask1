package com.soapserver.core.helpers;

import org.w3c.dom.NodeList;

public class NodeSetToStringUtil {

    public static String toString(NodeList nodeList) {
       StringBuilder builder = new StringBuilder();

        for (int i = 0; i < nodeList.getLength(); i++) {
            builder.append(nodeList.item(i).getTextContent());
            if (!(i == nodeList.getLength() - 1))
                builder.append(",");
        }

        return builder.toString();
    }
}
