package com.soapserver.core.filters.impl;

import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;
import com.soapserver.entities.LanguageType;

public class CheckTheCorrespondenceOfLanguagesFilter implements PreFilter<HotelsRequest> {

    private static final String EN_REGEX = "[a-zA-z]+";
    private static final String RU_REGEX = "[а-яА-я]+";

    @Override
    public boolean isApplicable(HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        final LanguageType language = request.getLanguage();
        final String cityName = request.getLocation().getCity();
        final String partOfHotelName = request.getPartOfHotelName();


        if (cityName != null) {
            if ((language == LanguageType.EN && !cityName.matches(EN_REGEX))
                    || (language == LanguageType.RU && !cityName.matches(RU_REGEX)))
                throw new ServiceException("Requested language and city name language are not corresponds");
        }

        if (partOfHotelName != null) {
            if ((language == LanguageType.EN && !partOfHotelName.matches(EN_REGEX))
                    || (language == LanguageType.RU && !partOfHotelName.matches(RU_REGEX)))
                throw new ServiceException("Requested language and hotel name language are not corresponds");
        }
    }
}
