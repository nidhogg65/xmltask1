package com.soapserver.core.filters.impl;

import com.soapserver.core.dao.CountriesDAO;
import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;
import java.util.List;

public class CheckCountryCodeFilter implements PreFilter<HotelsRequest> {

    private CountriesDAO countriesDAO;

    public void setCountriesDAO(CountriesDAO countriesDAO) {
        this.countriesDAO = countriesDAO;
    }

    @Override
    public boolean isApplicable(HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        final String requestCode = request.getLocation().getCountryCode();

        if (requestCode != null) {
            final List<String> countryCodes = countriesDAO.getCountriesCodes();
            if (!countryCodes.contains(requestCode))
                throw new ServiceException("Requested country code is incorrect");
        }
    }
}
