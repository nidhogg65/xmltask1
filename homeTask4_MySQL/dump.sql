-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: localhost    Database: sql_task
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gpt_hotel`
--

DROP TABLE IF EXISTS `gpt_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `category_id` enum('Hotel','Hostel','Apartments') NOT NULL,
  `chain_id` bigint(20) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `check_in` time NOT NULL,
  `check_out` time NOT NULL,
  `email` varchar(255) NOT NULL,
  `zip` varchar(50) NOT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `default_image` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `gpt_hotel_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `gpt_location` (`id`),
  CONSTRAINT `gpt_hotel_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `gpt_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel`
--

LOCK TABLES `gpt_hotel` WRITE;
/*!40000 ALTER TABLE `gpt_hotel` DISABLE KEYS */;
INSERT INTO `gpt_hotel` VALUES (2,'HH',3,2,'Hotel',123123123,'8 017 309-80-00','1231s12','www.booking.com/hotel/by/doubletree-by-hilton-minsk.ru.html','10:10:10','00:00:00','hotel@gmail.com','2313123',53.230000,12.320000,'',234234234,'2028-10-20 17:00:00'),(3,'PA',3,2,'Apartments',123123123,'8 017 449-84-22','1231s12','https://www.booking.com/hotel/by/prime-apartments-agency.ru.html','10:10:10','00:00:00','hotel@gmail.com','2313123',52.230000,12.310000,'',232234234,'2028-10-20 17:00:00'),(4,'HH',3,2,'Hotel',23123123,'8 017 329-84-22','1231s12','https://www.booking.com/hotel/by/hampton-by-hilton-minsk-city-centre.ru.html','10:10:10','00:00:00','hotel@gmail.com','2313123',52.230000,12.330000,'\0',132234234,'2028-10-20 17:00:00'),(5,'EA',4,2,'Apartments',2312123,'8 017 239-74-22','1231s12','https://www.booking.com/hotel/by/elite-apartments-grodno.ru.html','10:10:10','00:00:00','hotel@gmail.com','2313123',52.230000,11.330000,'',132234234,'2028-10-20 17:00:00'),(6,'WA',6,5,'Hotel',5312123,'8 017 231-24-22','1231s12','https://www.booking.com/hotel/it/warmth-hotel.ru.html','10:10:10','00:00:00','hotel@gmail.com','2313123',32.230000,10.330000,'',132234234,'2028-10-20 17:00:00'),(7,'PV',7,5,'Hotel',5312123,'8 017 261-14-72','1231s12','https://www.booking.com/hotel/it/palazzo-veneziano.ru.html','10:10:10','00:00:00','hotel@gmail.com','2313123',31.230000,11.330000,'',132234234,'2028-10-20 17:00:00'),(8,'CS',7,5,'Hotel',5312323,'8 017 999-24-72','1231s12','https://www.booking.com/hotel/it/casanova.ru.html','10:10:10','00:00:00','hotel@gmail.com','2313123',31.230000,11.330000,'',132234234,'2028-10-20 17:00:00'),(9,'HC',6,5,'Hotel',345346,'8 017 324-44-32','34fdsf3','https://www.booking.com/hotel/it/caravel.ru.html','10:10:10','00:00:00','HotelCaravel@gmail.com','234234',31.233200,11.332300,'',344353454,'2028-10-20 17:00:00'),(10,'PR',6,5,'Hotel',546546,'8 017 456-23-34','dsf332d','https://www.booking.com/hotel/it/poggioverde-roma.ru.html','10:10:10','00:00:00','HotelPogRoma@gmail.com','324234',31.233400,11.332200,'',132234234,'2028-10-20 17:00:00'),(11,'CP',4,2,'Apartments',3453455,'8 017 678-76-43','343434','https://www.booking.com/hotel/by/city-center-pavlovskogo-21.ru.html','10:10:10','00:00:00','citycenter@gmail.com','234234',52.233200,12.353430,'',345345345,'2028-10-20 17:00:00'),(12,'ER',7,5,'Hotel',3453544,'8 017 999-99-99','546456','https://www.booking.com/hotel/it/residenzacannaregio.ru.html','10:10:10','00:00:00','cannaregio@gmail.com','324234',52.233200,12.674300,'',345345345,'2028-10-20 17:00:00');
/*!40000 ALTER TABLE `gpt_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_image_type`
--

DROP TABLE IF EXISTS `gpt_hotel_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_image_type` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_image_type`
--

LOCK TABLES `gpt_hotel_image_type` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_image_type` DISABLE KEYS */;
INSERT INTO `gpt_hotel_image_type` VALUES (1,'general'),(2,'lobby'),(3,'canteen'),(4,'room'),(5,'bathroom');
/*!40000 ALTER TABLE `gpt_hotel_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_images`
--

DROP TABLE IF EXISTS `gpt_hotel_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) NOT NULL,
  `type_id` tinyint(3) NOT NULL,
  `url` varchar(600) NOT NULL,
  `thumbnail_url` varchar(600) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `gpt_hotel_images_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `gpt_hotel` (`id`),
  CONSTRAINT `gpt_hotel_images_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `gpt_hotel_image_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_images`
--

LOCK TABLES `gpt_hotel_images` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_images` DISABLE KEYS */;
INSERT INTO `gpt_hotel_images` VALUES (1,2,1,'https://www.booking.com','https://www.booking.com'),(2,2,3,'https://www.booking.com','https://www.booking.com'),(3,3,3,'https://www.booking.com','https://www.booking.com'),(4,4,1,'https://www.booking.com','https://www.booking.com'),(5,4,3,'https://www.booking.com','https://www.booking.com'),(6,4,5,'https://www.booking.com','https://www.booking.com'),(7,5,2,'https://www.booking.com','https://www.booking.com'),(8,5,3,'https://www.booking.com','https://www.booking.com'),(9,6,3,'https://www.booking.com','https://www.booking.com'),(10,6,4,'https://www.booking.com','https://www.booking.com'),(11,6,5,'https://www.booking.com','https://www.booking.com'),(12,7,5,'https://www.booking.com','https://www.booking.com'),(13,8,1,'https://www.booking.com','https://www.booking.com'),(14,6,1,'https://www.booking.com','https://www.booking.com'),(15,4,1,'https://www.booking.com','https://www.booking.com'),(16,9,1,'https://www.booking.com','https://www.booking.com'),(17,9,2,'https://www.booking.com','https://www.booking.com'),(18,9,4,'https://www.booking.com','https://www.booking.com'),(19,10,3,'https://www.booking.com','https://www.booking.com'),(20,10,4,'https://www.booking.com','https://www.booking.com'),(21,11,5,'https://www.booking.com','https://www.booking.com'),(22,12,1,'https://www.booking.com','https://www.booking.com'),(23,12,2,'https://www.booking.com','https://www.booking.com'),(24,12,3,'https://www.booking.com','https://www.booking.com'),(25,12,4,'https://www.booking.com','https://www.booking.com'),(26,12,5,'https://www.booking.com','https://www.booking.com');
/*!40000 ALTER TABLE `gpt_hotel_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_name`
--

DROP TABLE IF EXISTS `gpt_hotel_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_name` (
  `hotel_id` bigint(20) NOT NULL,
  `lang_id` tinyint(3) NOT NULL,
  `name` varchar(500) NOT NULL,
  KEY `hotel_id` (`hotel_id`),
  KEY `lang_id` (`lang_id`),
  CONSTRAINT `gpt_hotel_name_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `gpt_hotel` (`id`),
  CONSTRAINT `gpt_hotel_name_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `gpt_language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_name`
--

LOCK TABLES `gpt_hotel_name` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_name` DISABLE KEYS */;
INSERT INTO `gpt_hotel_name` VALUES (2,1,'DoubleTree by Hilton'),(2,2,'ДаблТри бай Хилтон'),(3,1,'Prime Apartments 1'),(3,2,'Прайп Апартментс 1'),(4,1,'Hampton by Hilton Minsk City Centre'),(4,2,'Хэмптон бай Хилтон Минск Сити Сентре'),(5,1,'Elite Apartments Grodno'),(6,1,'Warmthotel'),(6,2,'Вармтхотел'),(7,1,'Palazzo Veneziano'),(7,2,'Палацо Венециано'),(5,2,'Элит Апартментс Гродно'),(8,1,'Hotel Casanova'),(8,2,'Хотел Казанова'),(9,1,'Hotel Caravel'),(9,2,'Хотел Каравел'),(10,1,'Hotel Poggioverde Roma'),(10,2,'Хотел Поггиоверде Рома'),(11,1,'City Center Pavlovskogo 21'),(11,2,'Сити Сентер Павловского 21'),(12,1,'Eurostars Residenza Cannaregio'),(12,2,'Евростарс Резиденца Каннарегио');
/*!40000 ALTER TABLE `gpt_hotel_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_language`
--

DROP TABLE IF EXISTS `gpt_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_language` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_language`
--

LOCK TABLES `gpt_language` WRITE;
/*!40000 ALTER TABLE `gpt_language` DISABLE KEYS */;
INSERT INTO `gpt_language` VALUES (1,'en','english'),(2,'ru','russian');
/*!40000 ALTER TABLE `gpt_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location`
--

DROP TABLE IF EXISTS `gpt_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `type_id` tinyint(3) NOT NULL,
  `parent` bigint(20) DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `time_zone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `gpt_location_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `gpt_location_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location`
--

LOCK TABLES `gpt_location` WRITE;
/*!40000 ALTER TABLE `gpt_location` DISABLE KEYS */;
INSERT INTO `gpt_location` VALUES (1,'ROOT',1,NULL,'',123.000000,123.000000,NULL),(2,'BLR',2,1,'',54.000000,27.000000,NULL),(3,'BY-HM',3,2,'',54.000000,27.000000,NULL),(4,'BY-HR',3,2,'',53.000000,23.000000,NULL),(5,'ITA',2,1,'',42.000000,12.000000,NULL),(6,'IT-RM',3,5,'',42.000000,13.000000,NULL),(7,'IT-VE',3,5,'',45.000000,12.000000,NULL);
/*!40000 ALTER TABLE `gpt_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_name`
--

DROP TABLE IF EXISTS `gpt_location_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_name` (
  `location_id` bigint(20) NOT NULL,
  `lang_id` tinyint(3) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `location_id` (`location_id`),
  KEY `lang_id` (`lang_id`),
  CONSTRAINT `gpt_location_name_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `gpt_location` (`id`),
  CONSTRAINT `gpt_location_name_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `gpt_language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_name`
--

LOCK TABLES `gpt_location_name` WRITE;
/*!40000 ALTER TABLE `gpt_location_name` DISABLE KEYS */;
INSERT INTO `gpt_location_name` VALUES (2,1,'Belarus'),(2,2,'Беларусь'),(3,1,'Minsk'),(3,2,'Минск'),(4,1,'Grodno'),(4,2,'Гродно'),(5,1,'Italy'),(5,2,'Италия'),(6,1,'Rome'),(6,2,'Рим'),(7,1,'Venice'),(7,2,'Венеция');
/*!40000 ALTER TABLE `gpt_location_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_type`
--

DROP TABLE IF EXISTS `gpt_location_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_type` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_type`
--

LOCK TABLES `gpt_location_type` WRITE;
/*!40000 ALTER TABLE `gpt_location_type` DISABLE KEYS */;
INSERT INTO `gpt_location_type` VALUES (1,'root'),(2,'country'),(3,'city');
/*!40000 ALTER TABLE `gpt_location_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-06 15:46:12
