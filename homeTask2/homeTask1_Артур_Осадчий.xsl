<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    
    <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
    
    <xsl:template match="/h:Houses">
        <AllRooms>
            <xsl:for-each select="h:House">
                <xsl:sort select="@City"/>
                <xsl:sort select="i:Address"/>
                
                <xsl:variable name="city" select="@City"/>
                <xsl:variable name="address" select="i:Address"/>
                
                    <xsl:for-each select="h:Blocks/h:Block">
                        <xsl:sort select="@number"/>
                        
                        <xsl:variable name="block_number" select="@number"/>
                         
                        <xsl:for-each select=".//r:Room">
                            <xsl:sort select="@nuber" data-type="number"/>
                            <xsl:variable name="room_number" select="@nuber"/>
                            <xsl:variable name="room_guests" select="@guests"/>
                          
                            <xsl:variable name="HouseRoomsCount" select="count(ancestor::h:House//r:Room)"/>
                            <xsl:variable name="BlockRoomsCount" select="count(ancestor::h:Block//r:Room)"/>
                            <xsl:variable name="HouseGuestsCount" select="sum(ancestor::h:House//@guests)"/>
                            <xsl:variable name="GuestsPerRoomAverage" select="floor($HouseGuestsCount div $HouseRoomsCount)"/>
                            
                            <Room>
                                <Address>
                                    <xsl:value-of select="concat(translate($city, $lowercase, $uppercase), '/', 
                                        $address, '/', $block_number, '/', $room_number)"/>
                                </Address>
                                <HouseRoomsCount>
                                    <xsl:value-of select="$HouseRoomsCount"/>
                                </HouseRoomsCount>
                                <BlockRoomsCount>
                                    <xsl:value-of select="$BlockRoomsCount"/>
                                </BlockRoomsCount>
                                <HouseGuestsCount>
                                    <xsl:value-of select="$HouseGuestsCount"/>
                                </HouseGuestsCount>
                                <GuestsPerRoomAverage>
                                    <xsl:value-of select="$GuestsPerRoomAverage"/>
                                </GuestsPerRoomAverage>
                                <Allocated Single="{@guests = 1}" Double="{@guests = 2}" Triple="{@guests = 3}"
                                    Quarter="{@guests = 4}"/>
                            </Room>
                        </xsl:for-each>
                    </xsl:for-each>
            </xsl:for-each>
        </AllRooms>>
    </xsl:template>
</xsl:stylesheet>