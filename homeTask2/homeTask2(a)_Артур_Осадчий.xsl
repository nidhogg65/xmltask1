<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="Guests">
		<xsl:element name="string">
			<xsl:for-each select="Guest">
				<xsl:variable name="attributes" select="@*"/>
				<xsl:for-each select="$attributes">
					<xsl:value-of select="concat(., ',')"/>
				</xsl:for-each>
				<xsl:value-of select="concat(Type, ',', Profile/Address, '|')"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
