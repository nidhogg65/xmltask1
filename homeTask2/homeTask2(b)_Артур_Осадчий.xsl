<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/">
		<xsl:element name="Guests">
			<!--Строка до символа '|' (является элементом Guest)-->
			<xsl:variable name="guest1"><xsl:value-of select="substring-before(string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest1, ',')"/></xsl:attribute>
					
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest1, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
					
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
					
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
					
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
					
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
			<!--Строка после символа '|' (до конца строки, сохраняется для дальнейшего парсинга)-->
			<xsl:variable name="guest2_string"><xsl:value-of select="substring-after(string, '|')"/></xsl:variable>
			<xsl:variable name="guest2"><xsl:value-of select="substring-before($guest2_string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
				
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:variable name="guest3_string"><xsl:value-of select="substring-after($guest2_string, '|')"/></xsl:variable>
			<xsl:variable name="guest3"><xsl:value-of select="substring-before($guest3_string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
				
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:variable name="guest4_string"><xsl:value-of select="substring-after($guest3_string, '|')"/></xsl:variable>
			<xsl:variable name="guest4"><xsl:value-of select="substring-before($guest4_string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest4, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
				
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:variable name="guest5_string"><xsl:value-of select="substring-after($guest4_string, '|')"/></xsl:variable>
			<xsl:variable name="guest5"><xsl:value-of select="substring-before($guest5_string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest5, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest5, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
				
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:variable name="guest6_string"><xsl:value-of select="substring-after($guest5_string, '|')"/></xsl:variable>
			<xsl:variable name="guest6"><xsl:value-of select="substring-before($guest6_string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest6, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest6, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
				
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:variable name="guest7_string"><xsl:value-of select="substring-after($guest6_string, '|')"/></xsl:variable>
			<xsl:variable name="guest7"><xsl:value-of select="substring-before($guest7_string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest7, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest7, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
				
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:variable name="guest8_string"><xsl:value-of select="substring-after($guest7_string, '|')"/></xsl:variable>
			<xsl:variable name="guest8"><xsl:value-of select="substring-before($guest8_string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest8, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest8, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
				
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:variable name="guest9_string"><xsl:value-of select="substring-after($guest8_string, '|')"/></xsl:variable>
			<xsl:variable name="guest9"><xsl:value-of select="substring-before($guest9_string, '|')"/></xsl:variable>
			<xsl:element name="Guest">
				<xsl:attribute name="Age"><xsl:value-of select="substring-before($guest9, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest2"><xsl:value-of select="substring-after($guest9, ',')"/></xsl:variable>
				<xsl:attribute name="Nationalty"><xsl:value-of select="substring-before($attr_guest2, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest3"><xsl:value-of select="substring-after($attr_guest2, ',')"/></xsl:variable>
				<xsl:attribute name="Gender"><xsl:value-of select="substring-before($attr_guest3, ',')"/></xsl:attribute>
				
				<xsl:variable name="attr_guest4"><xsl:value-of select="substring-after($attr_guest3, ',')"/></xsl:variable>
				<xsl:attribute name="Name"><xsl:value-of select="substring-before($attr_guest4, ',')"/></xsl:attribute>
				
				<xsl:variable name="el_guest1"><xsl:value-of select="substring-after($attr_guest4, ',')"/></xsl:variable>
				<xsl:element name="Type"><xsl:value-of select="substring-before($el_guest1, ',')"/></xsl:element>
				
				<xsl:variable name="el_guest2"><xsl:value-of select="substring-after($el_guest1, ',')"/></xsl:variable>
				<xsl:element name="Profile">
					<xsl:element name="Address"><xsl:value-of select="$el_guest2"/></xsl:element>
				</xsl:element>
			</xsl:element>
			
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
